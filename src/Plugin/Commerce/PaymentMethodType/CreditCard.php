<?php

namespace Drupal\commerce_bancard\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the credit card payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "bancard_cc",
 *   label = @Translation("Bancard Credit card"),
 * )
 */
class CreditCard extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $args = [
      '@bc_type' => $payment_method->bc_type->value,
      '@bc_number' => $payment_method->bc_number->value,
    ];
    return $this->t('@bc_type starts/ends with @bc_number', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['bc_type'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Card type'))
      ->setDescription(t('The credit card type.'));

    $fields['bc_number'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Card number'))
      ->setDescription(t('The last few digits of the credit card number'));

    return $fields;
  }

}
