<?php

namespace Drupal\commerce_bancard\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_bancard\ApiClient;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @CommercePaymentGateway(
 *   id = "bancard_onsite_checkout",
 *   label = @Translation("Bancard OnSite"),
 *   display_label = @Translation("Bancard Payment Onsite"),
 *   forms = {
 *    "offsite-payment" = "Drupal\commerce_bancard\PluginForm\BancardOffsite\StoredPaymentMethodForm",
 * },
 * modes= {
 *     "staging" = "Staging",
 *     "production" = "Production"
 * },
 * payment_method_types = {"bancard_cc"},
 * requires_billing_information = FALSE,
 * )
 */
class BancardStoredPaymentGateway extends OffsitePaymentGatewayBase implements BancardStoredPaymentGatewayInterface {

    use StringTranslationTrait;

    /**
     * The checkout order manager.
     *
     * @var CheckoutOrderManagerInterface
     */
    protected $checkoutOrderManager;

    /**
     * The state manager.
     *
     * @var StateInterface
     */
    protected $state;

    /**
     * The Bancard gateway used for making API calls.
     *
     * @var Drupal\commerce_bancard\ApiClient
     */
    protected $api;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, CheckoutOrderManagerInterface $checkout_order_manager, StateInterface $state) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

        $this->checkoutOrderManager = $checkout_order_manager;
        $this->state = $state;
        $this->api = new ApiClient();
        if ($this->getMode() == 'production') {
            $this->api->stagingMode(false);
        } else {
            $this->api->stagingMode(true);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
                $configuration,
                $plugin_id,
                $plugin_definition,
                $container->get('entity_type.manager'),
                $container->get('plugin.manager.commerce_payment_type'),
                $container->get('plugin.manager.commerce_payment_method_type'),
                $container->get('datetime.time'),
                $container->get('commerce_checkout.checkout_order_manager'),
                $container->get('state')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPublicKey() {
        return $this->configuration['public_key'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPrivateKey() {
        return $this->configuration['private_key'];
    }

    /**
     * @inheritdoc
     */
    public function defaultConfiguration() {
        return [
            'public_key' => 'yFqqXJh9PqnugfrsdfffDff3sf556',
            'private_key' => '0808sEWdffASvvdd355sskl',
                ] + parent::defaultConfiguration();
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);

        $form['public_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Public key'),
            '#default_value' => $this->configuration['public_key'],
            '#description' => $this->t('Public key assigned during the registration process with Bancard.'),
            '#required' => true,
        ];
        $form['private_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Private key'),
            '#default_value' => $this->configuration['private_key'],
            '#description' => $this->t('Private key provided during the registration process with Bancard.'),
            '#required' => true,
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
        parent::submitConfigurationForm($form, $form_state);

        if (!$form_state->getErrors()) {
            $values = $form_state->getValue($form['#parents']);
            $this->configuration['public_key'] = $values['public_key'];
            $this->configuration['private_key'] = $values['private_key'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createPayment(PaymentInterface $payment, $capture = TRUE) {
        $logger = \Drupal::logger('commerce_bancard');
        $logger->info('createPayment()');
        $this->assertPaymentState($payment, ['new']);

        $order_id = $payment->getOrderId();
        $order = Order::load($order_id);

        $get_user_cards = $this->userCardsList($order);
        $transaction_status = $get_user_cards['status'];
        $transaction_data = $get_user_cards['data'];

        if ($transaction_status && $transaction_data['status'] === 'success') {
            $cards = $transaction_data['cards'];
            $payment_method_id = $payment->getPaymentMethodId();

            $alias_token = NULL;

            foreach ($cards as &$value) {
                if ($value['card_id'] == $payment_method_id) {
                    $alias_token = $value['alias_token'];
                }
            }

            if ($alias_token != NULL) {
                $this->doCreatePayment($order, $alias_token, FALSE);
            } else {
                throw new PaymentGatewayException('createPayment(): alias_token is null.');
            }
        } else {
            $this->messenger()->addWarning($this->t('Error processing your card'));
            throw new PaymentGatewayException('createPayment(): error findUserCards.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
        $payment_method->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function onCardNotify(OrderInterface $order, Request $request) {
        $logger = \Drupal::logger('commerce_bancard');
        $logger->info('onCardNotify(): Request content: ' . $request->getContent());

        if (!$response_data = json_decode($request->getContent(), true)) {
            throw new PaymentGatewayException('Notify: Response data missing, aborting.');
        }

        if ($response_data['status'] === 'add_new_card_success') {
            $get_user_cards = $this->userCardsList($order);
            $transaction_status = $get_user_cards['status'];
            $transaction_data = $get_user_cards['data'];

            if ($transaction_status && $transaction_data['status'] === 'success') {
                $cards = $transaction_data['cards'];
                $card = $cards[0];
                $card_exp = $card['expiration_date'];
                $date = date_create_from_format("m/y", $card_exp);
                $month = date_format($date, "m");
                $year = date_format($date, "Y");
                $expires = CreditCard::calculateExpirationTimestamp($month, $year);

                $payment_method = \Drupal\commerce_payment\Entity\PaymentMethod::load($card['card_id']);
                $payment_method->set('bc_type', $card['card_brand']);
                $payment_method->set('bc_number', $card['card_masked_number']);
                $payment_method->setExpiresTime($expires);
                $payment_method->setReusable(TRUE);
                $payment_method->save();

                $alias_token = $card['alias_token'];

                $this->doCreatePayment($order, $alias_token, TRUE);
            } else {
                $this->messenger()->addWarning($this->t('Error get user cards.'));
            }
        } else {
            $this->messenger()->addWarning($this->t('The card has not been registered. To continue with the cadastre, please contact the Bancard CAC. *288/4161000.'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function onReturn(OrderInterface $order, Request $request) {
        $logger = \Drupal::logger('commerce_bancard');
        $logger->info('onReturn(): Request content: ' . $request->getContent());

        if (!$order->isPaid()) {
            throw new PaymentGatewayException('Return: order is not paid.');
        }
    }

    private function userCardsList(OrderInterface $order) {
        $user_id = $order->getCustomerId();

        $token = [
            'private_key' => $this->getPrivateKey(),
            'user_id' => $user_id,
            'request_user_cards' => 'request_user_cards',
        ];

        $data = [
            'public_key' => $this->getPublicKey(),
            'operation' => [
                'token' => $this->api->calculateChecksum($token),
            ],
        ];

        return $this->api->getUserCards($data, $user_id);
    }

    private function doCreatePayment(OrderInterface $order, $alias_token, bool $new_card) {
        $logger = \Drupal::logger('commerce_bancard');
        $order_id = $order->id();
        $logger->info('doCreatePayment(): Order ID ' . $order_id);

        $ammount = $order->getTotalPrice()->getNumber();
        $ammount_dec = number_format($ammount, 2, '.', '');
        $currency_code = $order->getTotalPrice()->getCurrencyCode();

        $token = [
            'private_key' => $this->getPrivateKey(),
            'shop_process_id' => $order_id,
            'charge' => 'charge',
            'amount' => $ammount_dec,
            'currency' => $currency_code,
            'alias_token' => $alias_token,
        ];

        $data = [
            'public_key' => $this->getPublicKey(),
            'operation' => [
                'token' => $this->api->calculateChecksum($token),
                'shop_process_id' => $order_id,
                'amount' => $ammount_dec,
                'number_of_payments' => '1',
                'currency' => $currency_code,
                'additional_data' => '',
                'description' => '',
                'alias_token' => $alias_token,
            ],
        ];

        $request_payment = $this->api->requestPayment($data);

        $transaction_status = $request_payment['status'];
        $transaction_data = $request_payment['data'];

        if ($transaction_status) {
            $received_token = $transaction_data['operation']['token'];

            $verify_token = $this->api->compareToken($received_token, $token);

            if ($verify_token) {
                $state_item = $order->get('state')->first();
                $current_state = $state_item->getValue();

                if ($current_state['value'] !== 'draft') {
                    $logger->info('Notify: Skipping order state transition, order already not in "draft" state anymore, current state: @current-state', ['@current-state' => $current_state['value']]);
                    return false;
                }

                if ($transitions = $state_item->getTransitions()) {
                    $state_item->applyTransition(current($transitions));
                    // Unlock the order if needed.
                    $order->isLocked() ? $order->unlock() : null;
                    $order->save();
                    $logger->info('Notify: Set transition successfully.');
                }

                $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
                $payment = $payment_storage->create([
                    'state' => 'authorization',
                    'amount' => $order->getTotalPrice(),
                    'payment_gateway' => $this->parentEntity->id(),
                    'order_id' => $order_id,
                    'remote_id' => $transaction_data['operation']['ticket_number'],
                    'remote_state' => 'success',
                ]);

                $payment->save();

                $this->messenger()->addStatus($this->t($transaction_data['operation']['response_description']));

                $logger->info('Notify: Payment information saved successfully. Transaction reference: ' . $order_id);
            } else {
                $this->messenger()->addWarning($this->t('Error there was a problem with your payment..'));
                if ($new_card) {
                    $logger->info('Return: Error token mismatch.');
                } else {
                    throw new PaymentGatewayException('Return: Error token mismatch.');
                }
            }
        } else {
            $this->messenger()->addWarning($this->t('Error there was a problem with your payment..'));
            $response_code = $transaction_data['operation']['response_code'];
            $this->getErrorMessage($response_code, $new_card);
        }
    }

    private function getErrorMessage($code, bool $new_card) {
        switch ($code) {
            case 05:
                $this->messenger()->addWarning($this->t('Card inabilited.'));
                $this->exeption($code, $new_card);
                break;
            case 12:
                $this->messenger()->addWarning($this->t('Invalid transaction.'));
                $this->exeption($code, $new_card);
                break;
            case 15:
                $this->messenger()->addWarning($this->t('Invalid card.'));
                $this->exeption($code, $new_card);
                break;
            case 51:
                $this->messenger()->addWarning($this->t('Insufficient funds.'));
                $this->exeption($code, $new_card);
                break;
            default:
                $this->messenger()->addWarning($this->t('Error payment was not successful.'));
                $this->exeption($code, $new_card);
        }
    }

    private function exeption($code, bool $new_card) {
        if (!$new_card) {
            throw new PaymentGatewayException('Return: Error there was a problem with the payment. CODE: ' . $code);
        }
    }
}
