<?php

namespace Drupal\commerce_bancard\Controller;

use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CardNotificationController implements ContainerInjectionInterface {

    /**
     * The checkout order manager.
     *
     * @var CheckoutOrderManagerInterface
     */
    protected $checkoutOrderManager;

    /**
     * The messenger.
     *
     * @var MessengerInterface
     */
    protected $messenger;

    /**
     * The logger.
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * The entity type manager.
     *
     * @var EntityTypeManagerInterface
     */
    protected EntityTypeManagerInterface $entityTypeManager;

    /**
     * Constructs a new PaymentCheckoutController object.
     *
     * @param CheckoutOrderManagerInterface $checkout_order_manager
     *   The checkout order manager.
     * @param MessengerInterface $messenger
     *   The messenger.
     * @param LoggerInterface $logger
     *   The logger.
     * @param EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     */
    public function __construct(CheckoutOrderManagerInterface $checkout_order_manager, MessengerInterface $messenger, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
        $this->checkoutOrderManager = $checkout_order_manager;
        $this->messenger = $messenger;
        $this->logger = $logger;
        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('commerce_checkout.checkout_order_manager'),
                $container->get('messenger'),
                $container->get('logger.channel.commerce_payment'),
                $container->get('entity_type.manager')
        );
    }

    /**
     * Provides the "notifyCard" for save user cards.
     *
     *
     * @param Request $request
     *   The request.
     * @param RouteMatchInterface $route_match
     *   The route match.
     */
    public function notifyCard(Request $request, RouteMatchInterface $route_match) {
        /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
        $order = $route_match->getParameter('commerce_order');
        $step_id = $route_match->getParameter('step');
        $this->validateStepId($step_id, $order);

        $order = $this->entityTypeManager->getStorage('commerce_order')->loadForUpdate($order->id());
        \Drupal::routeMatch()->getParameters()->set('commerce_order', $order);

        /** @var PaymentGatewayInterface $payment_gateway */
        $payment_gateway = $order->get('payment_gateway')->entity;
        $payment_gateway_plugin = $payment_gateway->getPlugin();

        $response = $payment_gateway_plugin->onCardNotify($order, $request);
        if (!$response) {
            $response = new Response('', 200);
        }

        return $response;
    }

    protected function validateStepId($requested_step_id, OrderInterface $order) {
        $step_id = $this->checkoutOrderManager->getCheckoutStepId($order);
        if ($requested_step_id != $step_id) {
            throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
                                'commerce_order' => $order->id(),
                                'step' => $step_id,
                            ])->toString());
        }
    }
}
