<?php

namespace Drupal\commerce_bancard\PluginForm\BancardOffsite;

use Drupal\commerce_bancard\ApiClient;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class PaymentForm extends BasePaymentOffsiteForm {

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     **/
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);

        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payment = $this->entity;
        /** @var \Drupal\commerce_bancard\Plugin\Commerce\PaymentGateway\BancardPaymentGatewayInterface $plugin */
        $plugin = $payment->getPaymentGateway()->getPlugin();

        $public_key = $plugin->getPublicKey();
        $private_key = $plugin->getPrivateKey();
        $gateway_mode = $plugin->getMode();

        $bancard = new ApiClient();

        if ($gateway_mode == 'production') {
            $bancard->stagingMode(false);
            $form['#attached']['library'][] = 'commerce_bancard/production';
        } else {
            $bancard->stagingMode(true);
            $form['#attached']['library'][] = 'commerce_bancard/staging';
        }
        $form['#attached']['library'][] = 'commerce_bancard/checkout';

        // Payment data.
        $payment_amount = $payment->getAmount()->getNumber();
        $order_id = $payment->getOrderId();
        $currency_code = $payment->getAmount()->getCurrencyCode();
        $order_ammount = number_format($payment_amount, 2, '.', '');

        $token = [
            'private_key' => $private_key,
            'shop_process_id' => $order_id,
            'amount' => $order_ammount,
            'currency' => $currency_code,
        ];

        $options = [
            'public_key' => $public_key,
            'operation' => [
                'token' => $bancard->calculateChecksum($token),
                'shop_process_id' => $order_id,
                'currency' => $currency_code,
                'amount' => $order_ammount,
                'additional_data' => '',
                'description' => '',
                'return_url' => $form['#return_url'],
                'cancel_url' => $form['#cancel_url'],
            ],
        ];

        $auth_transaction = $bancard->getPaymentForm($options);
        $form = $this->buildRedirectForm($form, $form_state, '', $auth_transaction, '');
        $transaction_status = $auth_transaction['status'];

        if ($transaction_status && $auth_transaction['data']['status'] === 'success') {
            $transaction_data = $auth_transaction['data'];
            $checkout_id = $transaction_data['process_id'];
            $form['#attached']['drupalSettings']['transactionData'] = $checkout_id;
        } else {
            \Drupal::messenger()->addWarning($this->t('There was a problem with payment gateway.'));
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function buildRedirectForm(array $form, FormStateInterface $form_state, $redirect_url, array $data, $redirect_method = BasePaymentOffsiteForm::REDIRECT_POST) {
        if ($data['status'] && $data['data']['status'] === 'success') {
            $help_message = $this->t('Please wait while the payment server loads. If nothing happens within 10 seconds, please click on the button below.');
            $form['commerce_message'] = [
                '#markup' => '<div class="checkout-help">' . $help_message . '</div>',
                '#weight' => -10,
                '#process' => [
                    [get_class($this), 'processRedirectForm'],
                ],
            ];
        } else {
            $back = $this->t('Go back');
            $cancel = $form['#cancel_url'];
            $help_message = $this->t('Authorization error. Contact the site administrator.');
            $form['commerce_message'] = [
                '#markup' => '<div class="checkout-help">' . $help_message . ' <a href="' . $cancel . '">' . $back . '</a></div>',
                '#weight' => -10,
            ];
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public static function processRedirectForm(array $element, FormStateInterface $form_state, array &$complete_form) {
        $complete_form['#attributes']['class'][] = 'payment-redirect-form';
        unset($element['#action']);
        // The form actions are hidden by default, but needed in this case.
        $complete_form['actions']['#access'] = true;

        foreach (Element::children($complete_form['actions']) as $element_name) {
            $complete_form['actions'][$element_name]['#access'] = true;
        }
        return $element;
    }
}
