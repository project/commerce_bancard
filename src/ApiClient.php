<?php

namespace Drupal\commerce_bancard;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

class ApiClient {

    protected static $_staging = false;
    protected $verifyCount = 0;
    const PRODUCTION_URL = "https://vpos.infonet.com.py/";
    const STAGING_URL = "https://vpos.infonet.com.py:8888/";

    public function stagingMode($status = false) {
        if ($status) {
            self::$_staging = true;
        }

    }

    public static function getBasePaymentURL() {
        if (self::$_staging) {
            return self::STAGING_URL;
        }

        return self::PRODUCTION_URL;
    }

    public function getPaymentForm($data): array {
        $logger = \Drupal::logger('commerce_bancard');
        $logger->notice('Api: Requesting payment form');

        $url = $this->getBasePaymentURL() . 'vpos/api/0.3/single_buy';

        $client = new Client();
        try {
            $api_request = $client->request('POST', $url,
                [
                    'json' => $data,
                ]);
        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while requesting payment form. Error: ' . $e->getMessage());
            return [
                'status' => false,
                'data' => $e->getMessage(),
            ];
        }

        $response = json_decode((string) $api_request->getBody(), true);

        return [
            'status' => true,
            'data' => $response,
        ];
    }
    
        public function requestPayment($data): array {
        $logger = \Drupal::logger('commerce_bancard');
        $logger->notice('Api: Requesting payment on site');

        $url = $this->getBasePaymentURL() . 'vpos/api/0.3/charge';

        $client = new Client();
        try {
            $api_request = $client->request('POST', $url,
                [
                    'json' => $data,
                ]);
        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while requesting payment on site. Error: ' . $e->getMessage());
            return [
                'status' => false,
                'data' => $e->getMessage(),
            ];
        }

        $response = json_decode((string) $api_request->getBody(), true);
        
        // Check the status is successful.
        if ($response) {
           $reference_number = $response['operation']['shop_process_id'];
            if ($response && $response['operation'] && $response['operation']['response_code'] === "00") {
                $logger->notice('Api: Payment on site a successful transaction. Transaction Reference: ' . $reference_number);
                return [
                    'status' => true,
                    'data' => $response,
                ];
            } elseif ($response && $response['operation'] && $response['operation']['response_code'] > "00") {
                $logger->warning('Api: Payment on site a failed transaction. Transaction Reference: ' . $reference_number . '. Response: ' . json_encode($response));
                return [
                    'status' => false,
                    'data' => $response,
                ];
            } else {
                // Handled an undecisive transaction. Probably timed out.
                $logger->warning('Api: Verified an undecisive transaction: ' . json_encode($response) . 'Transaction Reference: ' . $reference_number);
                if ($this->verifyCount > 4) {
                    // We couldn't get a status in 5 requeries.
                    $logger->warning('Api: Transaction verification timed out. Transaction Reference: ' . $reference_number);
                    return [
                        'status' => false,
                        'data' => $response,
                    ];
                } else {
                    $logger->notice('Api: Delaying next verification for 3 seconds. Transaction Reference: ' . $reference_number);
                    sleep(3);
                    $logger->notice('Api: Now retrying verification. Transaction Reference: ' . $reference_number);
                    $this->verifyTransaction($reference_number);
                }
            }
        } else {
            $logger->error('Api: Verify call returned error: ' . json_encode($response));
            return [
                'status' => false,
                'data' => $response,
            ];
        }

        return [
            'status' => true,
            'data' => $response,
        ];
    }
    
    public function getUserCards($data, $user_id): array {
        $logger = \Drupal::logger('commerce_bancard');
        $logger->notice('Api: Requesting user cards');

        $url = $this->getBasePaymentURL() . 'vpos/api/0.3/users/'. $user_id .'/cards';

        $client = new Client();
        try {
            $api_request = $client->request('POST', $url,
                [
                    'json' => $data,
                ]);
        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while requesting user cards. Error: ' . $e->getMessage());
            return [
                'status' => false,
                'data' => $e->getMessage(),
            ];
        }

        $response = json_decode((string) $api_request->getBody(), true);

        return [
            'status' => true,
            'data' => $response,
        ];
    }

    public function verifyTransaction($data, $reference_number): array {
        $this->verifyCount++;
        $logger = \Drupal::logger('commerce_bancard');
        $logger->notice('Api: Verifying Transaction: ' . $reference_number);

        $url = $this->getBasePaymentURL() . 'vpos/api/0.3/single_buy/confirmations';

        $client = new Client();
        try {
            $api_request = $client->request('POST', $url, ['json' => $data]);
        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while verifying transaction: ' . $reference_number . '. Error: ' . $e->getMessage());
            return [
                'status' => false,
                'data' => $e->getMessage(),
            ];
        }

        $response = json_decode((string) $api_request->getBody(), true);

        // Check the status is successful.
        if ($response && $response['status'] === "success") {
            if ($response && $response['confirmation'] && $response['confirmation']['response_code'] === "00") {
                $logger->notice('Api: Verified a successful transaction. Transaction Reference: ' . $reference_number);
                return [
                    'status' => true,
                    'data' => $response,
                ];
            } elseif ($response && $response['confirmation'] && $response['confirmation']['response_code'] > "00") {
                $logger->warning('Api: Verified a failed transaction. Transaction Reference: ' . $reference_number . '. Response: ' . json_encode($response));
                return [
                    'status' => false,
                    'data' => $response,
                ];
            } else {
                // Handled an undecisive transaction. Probably timed out.
                $logger->warning('Api: Verified an undecisive transaction: ' . json_encode($response) . 'Transaction Reference: ' . $reference_number);
                if ($this->verifyCount > 4) {
                    // We couldn't get a status in 5 requeries.
                    $logger->warning('Api: Transaction verification timed out. Transaction Reference: ' . $reference_number);
                    return [
                        'status' => false,
                        'data' => $response,
                    ];
                } else {
                    $logger->notice('Api: Delaying next verification for 3 seconds. Transaction Reference: ' . $reference_number);
                    sleep(3);
                    $logger->notice('Api: Now retrying verification. Transaction Reference: ' . $reference_number);
                    $this->verifyTransaction($reference_number);
                }
            }
        } else {
            $logger->error('Api: Verify call returned error: ' . json_encode($response) . 'Transaction Reference: ' . $reference_number);
            return [
                'status' => false,
                'data' => $response,
            ];
        }

        return [
            'status' => false,
            'data' => $response,
        ];
    }

    public function compareToken($received_token, $store_token) {
        $current_token = $this->calculateChecksum($store_token);

        if ($received_token === $current_token) {
            return true;
        }
        return false;
    }

    public function calculateChecksum(array $options) {
        $hashed_payload = '';

        foreach ($options as $value) {
            $hashed_payload .= $value;
        }

        return hash('md5', $hashed_payload);
    }
}
