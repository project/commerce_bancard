<?php

namespace Drupal\commerce_bancard\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;

interface BancardPaymentGatewayInterface extends SupportsAuthorizationsInterface, SupportsNotificationsInterface {
    /**
     * Get the configured Bancard API Public key.
     *
     * @return string
     *   The Bancard API Public key.
     */
    public function getPublicKey();
    /**
     * Get the configured Bancard API Private key.
     *
     * @return string
     *   The Bancard API Private key.
     */
    public function getPrivateKey();

}
