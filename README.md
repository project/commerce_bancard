# Commerce Bancard
Allows merchants of Paraguay to accept payments with [Bancard](https://www.bancard.com.py/) for Drupal 9, 10 [Commerce 2x](https://www.drupal.org/project/commerce).
## Get Support
Visit [our site](https://jaranetwork.com) for more information about the project.