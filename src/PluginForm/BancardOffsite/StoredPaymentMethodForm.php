<?php

namespace Drupal\commerce_bancard\PluginForm\BancardOffsite;

use Drupal\commerce_bancard\ApiClient;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class StoredPaymentMethodForm extends BasePaymentOffsiteForm {

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);
        /** @var PaymentInterface $payment */
        $payment = $this->entity;
        $payment_gateway = $payment->getPaymentGateway();
        $order = $payment->getOrder();

        // Bancard requires to include a card id in the request.
        // Look up a card id if there was a previous registration error.
        $user_payment_method = $this->getUnusedCardId($order->getCustomer(), $payment->getPaymentGateway());

        $card_id = NULL;

        if (!empty($user_payment_method)) {
            foreach ($user_payment_method as &$value) {
                if ($value->getRemoteId() == NULL) {
                    $card_id = $value->id();
                    break;
                }
            }
        }

        $user_id = $order->getCustomerId();
        // Create a new card id.
        if ($card_id == NULL) {
            $payment_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment_method');
            assert($payment_method_storage instanceof PaymentMethodStorageInterface);
            $payment_method = $payment_method_storage->createForCustomer(
                    'bancard_cc',
                    $payment_gateway->id(),
                    $user_id,
                    $order->getBillingProfile()
            );
            // setReusable(FALSE) to hide in the list of customer payment methods
            $payment_method->setReusable(FALSE);
            $payment_method->save();
            $card_id = $payment_method->id();
        }

        $plugin = $payment_gateway->getPlugin();
        $gateway_mode = $plugin->getMode();

        $bancard = new ApiClient();

        if ($gateway_mode == 'production') {
            $bancard->stagingMode(false);
            $form['#attached']['library'][] = 'commerce_bancard/bancard_stored_production';
        } else {
            $bancard->stagingMode(true);
            $form['#attached']['library'][] = 'commerce_bancard/bancard_stored_staging';
        }
        $form['#attached']['library'][] = 'commerce_bancard/bancard_stored_checkout';

        $token = [
            'private_key' => $plugin->getPrivateKey(),
            'card_id' => $card_id,
            'user_id' => $user_id,
            'request_new_card' => 'request_new_card',
        ];

        $options = [
            'public_key' => $plugin->getPublicKey(),
            'operation' => [
                'token' => $bancard->calculateChecksum($token),
                'card_id' => $card_id,
                'user_id' => $user_id,
                'user_cell_phone' => $order->getBillingProfile()->get('field_phone_number')->value,
                'user_mail' => $order->getEmail(),
                'return_url' => $form['#return_url'],
            ],
        ];

        $auth_transaction = $bancard->getPaymentForm($options);
        $form = $this->buildRedirectForm($form, $form_state, '', $auth_transaction, '');
        $transaction_status = $auth_transaction['status'];

        if ($transaction_status && $auth_transaction['data']['status'] === 'success') {
            $transaction_data = $auth_transaction['data'];
            $checkout_id = $transaction_data['process_id'];
            $form['#attached']['drupalSettings']['transactionData'] = $checkout_id;
        } else {
            \Drupal::messenger()->addWarning($this->t('There was a problem with payment gateway.'));
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function buildRedirectForm(array $form, FormStateInterface $form_state, $redirect_url, array $data, $redirect_method = BasePaymentOffsiteForm::REDIRECT_POST) {
        if ($data['status'] && $data['data']['status'] === 'success') {
            $help_message = $this->t('Dear user, </br> You will then go through for only this time a process of
validation with security questions. To start please have in
consider the following recommendations: </br> 1- Verify your identity card number. </br> 2- Have your active credit and debit cards handy </br> 3- Check the amount and place of your last purchases in
shops or ATM withdrawals');
            $form['commerce_message'] = [
                '#markup' => '<div class="checkout-help">' . $help_message . '</div>',
                '#weight' => -10,
                '#process' => [
                    [get_class($this), 'processRedirectForm'],
                ],
            ];
        } else {
            $back = $this->t('Go back');
            $cancel = $form['#cancel_url'];
            $help_message = $this->t('Authorization error. Contact the site administrator.');
            $form['commerce_message'] = [
                '#markup' => '<div class="checkout-help">' . $help_message . ' <a href="' . $cancel . '">' . $back . '</a></div>',
                '#weight' => -10,
            ];
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public static function processRedirectForm(array $element, FormStateInterface $form_state, array &$complete_form) {
        $complete_form['#attributes']['class'][] = 'payment-redirect-form';
        unset($element['#action']);
        // The form actions are hidden by default, but needed in this case.
        $complete_form['actions']['#access'] = true;

        foreach (Element::children($complete_form['actions']) as $element_name) {
            $complete_form['actions'][$element_name]['#access'] = true;
        }

        return $element;
    }

    private function getUnusedCardId($account, $payment_gateway) {
        $payment_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment_method');
        $query = $payment_method_storage->getQuery();
        $query
                ->condition('uid', $account->id())
                ->condition('payment_gateway', $payment_gateway->id())
                ->condition('payment_gateway_mode', $payment_gateway->getPlugin()->getMode())
                ->condition('reusable', FALSE)
                ->accessCheck(FALSE)
                ->sort('method_id', 'ASC');
        $result = $query->execute();
        if (empty($result)) {
            return [];
        } else {
            return $payment_method_storage->loadMultiple($result);
        }
    }
}
