<?php

namespace Drupal\commerce_bancard\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_bancard\ApiClient;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @CommercePaymentGateway(
 *   id = "zimple_redirect_checkout",
 *   label = @Translation("Zimple OffSite"),
 *   display_label = @Translation("Zimple Payment"),
 *   forms = {
 *    "offsite-payment" = "Drupal\commerce_bancard\PluginForm\ZimpleOffsite\ZimplePaymentForm",
 *    "capture-payment" = "Drupal\commerce_payment\PluginForm\PaymentCaptureForm",
 *    "void-payment" = "Drupal\commerce_payment\PluginForm\PaymentVoidForm",
 * },
 * modes= {
 *     "staging" = "Staging",
 *     "production" = "Production"
 * },
 * payment_method_types = {"credit_card"},
 * credit_card_types = {
 *   "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 * },
 * )
 */
class ZimplePaymentGateway extends OffsitePaymentGatewayBase implements BancardPaymentGatewayInterface {

    use StringTranslationTrait;

    /**
     * The checkout order manager.
     *
     * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
     */
    protected $checkoutOrderManager;

    /**
     * The state manager.
     *
     * @var \Drupal\Core\State\StateInterface
     */
    protected $state;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, CheckoutOrderManagerInterface $checkout_order_manager, StateInterface $state) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

        $this->checkoutOrderManager = $checkout_order_manager;
        $this->state = $state;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('entity_type.manager'),
            $container->get('plugin.manager.commerce_payment_type'),
            $container->get('plugin.manager.commerce_payment_method_type'),
            $container->get('datetime.time'),
            $container->get('commerce_checkout.checkout_order_manager'),
            $container->get('state')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPublicKey() {
        return $this->configuration['public_key'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPrivateKey() {
        return $this->configuration['private_key'];
    }

    /**
     * @inheritdoc
     */
    public function defaultConfiguration() {
        return [
            'public_key' => 'yFqqXJh9PqnugfrsdfffDff3sf556',
            'private_key' => '0808sEWdffASvvdd355sskl',
        ] + parent::defaultConfiguration();
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);

        $form['public_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Public key'),
            '#default_value' => $this->configuration['public_key'],
            '#description' => $this->t('Public key assigned during the registration process with Zimple.'),
            '#required' => true,
        ];
        $form['private_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Private key'),
            '#default_value' => $this->configuration['private_key'],
            '#description' => $this->t('Private key provided during the registration process with Zimple.'),
            '#required' => true,
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
        parent::submitConfigurationForm($form, $form_state);

        if (!$form_state->getErrors()) {
            $values = $form_state->getValue($form['#parents']);
            $this->configuration['public_key'] = $values['public_key'];
            $this->configuration['private_key'] = $values['private_key'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildPaymentOperations(PaymentInterface $payment) {
        $operations = [];
        $operations['capture'] = [
            'title' => $this->t('Capture'),
            'page_title' => $this->t('Capture payment'),
            'plugin_form' => 'capture-payment',
            'access' => $this->canCapturePayment($payment),
        ];
        $operations['void'] = [
            'title' => $this->t('Void'),
            'page_title' => $this->t('Void payment'),
            'plugin_form' => 'void-payment',
            'access' => $this->canVoidPayment($payment),
        ];

        return $operations;
    }

    /**
     * {@inheritdoc}
     */
    public function canCapturePayment(PaymentInterface $payment) {
        return $payment->getState()->getId() === 'authorization';
    }

/**
 * {@inheritdoc}
 */
    public function capturePayment(PaymentInterface $payment, Price $amount = null) {
        $this->assertPaymentState($payment, ['authorization']);

        // If not specified, use the entire amount.
        $amount = $amount ?: $payment->getAmount();
        $payment->state = 'completed';
        $payment->setAmount($amount);
        $payment->save();
    }

    /**
     * {@inheritdoc}
     */
    public function canVoidPayment(PaymentInterface $payment){
        return $payment->getState()->getId() === 'authorization';
    }

    /**
     * {@inheritdoc}
     */
    public function voidPayment(PaymentInterface $payment) {
        $this->assertPaymentState($payment, ['authorization']);

        $payment->state = 'voided';
        $payment->save();
    }

    /**
     * {@inheritdoc}
     */
    public function onNotify(Request $request) {
        $logger = \Drupal::logger('commerce_bancard');

        if (!$response_data = json_decode($request->getContent(), true)) {
            throw new PaymentGatewayException('Notify: Response data missing, aborting.');
        }

        if (empty($response_data['operation']['shop_process_id'])) {
            throw new PaymentGatewayException('Invoice id missing, aborting.');
        }
        /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
        if (!$order = Order::load($response_data['operation']['shop_process_id'])) {
            throw new PaymentGatewayException('Could not find matching order.');
        }

        if (!$received_token = $response_data['operation']['token']) {
            throw new PaymentGatewayException('Notify: Could not find matching token.');
        }

        $public_key = $this->getPublicKey();
        $private_key = $this->getPrivateKey();
        $zimple = new ApiClient();

        if ($this->getMode() == 'production') {
            $zimple->stagingMode(false);
        } else {
            $zimple->stagingMode(true);
        }
        //Check the match between the token sent and the token received.
        $order_id = $order->id();
        $ammount = $order->getTotalPrice()->getNumber();
        $ammount_dec = number_format($ammount, 2, '.', '');
        $currency_code = $order->getTotalPrice()->getCurrencyCode();

        $store_token = [
            'private_key' => $private_key,
            'shop_process_id' => $order_id,
            'amount' => $ammount_dec,
            'currency' => $currency_code,
        ];

        $verify_token = $zimple->compareToken($received_token, $store_token);

        if ($verify_token) {
            $token = [
                'private_key' => $private_key,
                'shop_process_id' => $order_id,
                'get_confirmation' => 'get_confirmation',
            ];

            $data = [
                'public_key' => $public_key,
                'operation' => [
                    'token' => $zimple->calculateChecksum($token),
                    'shop_process_id' => $order->id(),
                ],
            ];
            
            $verify_transaction = $zimple->verifyTransaction($data, $order_id);

            $transaction_status = $verify_transaction['status'];
            $transaction_data = $verify_transaction['data'];
            $msg = $transaction_data['messages'];

            if ($transaction_status) {
                $charged_amount = $transaction_data['confirmation']['amount'];

                if ($ammount_dec == $charged_amount) {
                    $state_item = $order->get('state')->first();
                    $current_state = $state_item->getValue();

                    if ($current_state['value'] !== 'draft') {
                        $logger->info('Notify: Skipping order state transition, order already not in "draft" state anymore, current state: @current-state', ['@current-state' => $current_state['value']]);
                        return false;
                    }

                    if ($transitions = $state_item->getTransitions()) {
                        $state_item->applyTransition(current($transitions));
                        // Unlock the order if needed.
                        $order->isLocked() ? $order->unlock() : null;
                        $order->save();
                        $logger->info('Notify: Set transition successfully.');
                    }

                    $zimp_ref = $transaction_data['confirmation']['ticket_number'];
                    $status = $transaction_data['status'];

                    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
                    $payment = $payment_storage->create([
                        'state' => 'authorization',
                        'amount' => $order->getTotalPrice(),
                        'payment_gateway' => $this->parentEntity->id(),
                        'order_id' => $order_id,
                        'remote_id' => $zimp_ref,
                        'remote_state' => $status,
                    ]);

                    $payment->save();

                    $this->messenger()->addStatus($this->t($transaction_data['confirmation']['response_description']));

                    $logger->info('Notify: Payment information saved successfully. Transaction reference: ' . $order_id);

                } else {
                    $logger->warning('Notify: Charged Amount is: ' . $charged_amount . ' while Order Amount: ' . $ammount_dec);
                    $this->messenger()->addWarning($this->t('There was a problem with your payment..'));
                }
            } else {
                $logger->warning('Notify: Zimple Response Code: ' . $transaction_data['confirmation']['response_code'] . ' Order ID: ' . $order_id);
                $response_code = $transaction_data['confirmation']['response_code'];
                $this->getMessage($response_code);
            }
        } else {
            $this->messenger()->addWarning($this->t('Error there was a problem with your payment..'));
        }
    }
    /**
     * {@inheritdoc}
     */
    public function onReturn(OrderInterface $order, Request $request) {
        $logger = \Drupal::logger('commerce_bancard');
        $logger->info('Return: Request content: ' . $request->getContent());

        if (!$order->isPaid()) {
            throw new PaymentGatewayException('Return: The order is not paid.');
        }
    }

    private function getMessage($code) {
        switch ($code) {
            case 05:
                $this->messenger()->addWarning($this->t('Card inabilited.'));
                break;
            case 12:
                $this->messenger()->addWarning($this->t('Invalid transaction.'));
                break;
            case 15:
                $this->messenger()->addWarning($this->t('Invalid card.'));
                break;
            case 51:
                $this->messenger()->addWarning($this->t('Insufficient funds.'));
                break;
            default:
                $this->messenger()->addWarning($this->t('Error payment was not successful.'));
        }
    }
}
