(function ($, Drupal, drupalSettings, once) {
    'use strict';
  
    Drupal.behaviors.offsiteForm = {
              attach: function (context) {
                once('offsiteForm', 'html').forEach(function (element) {
                  const options = drupalSettings.transactionData;

                  if(options) {
                    let html = `
                    <div id="bModal" class="b-modal">
                      <div class="b-modal-content">
                        <span class="b-close">&times;</span>
                          <div id="iframe-container">
                        </div>
                      </div>
                    </div>
                    `;
                    $('.checkout-pane').append(html);

                    var styles = {
                        "form-background-color": "#001b60",
                        "button-background-color": "#4faed1",
                        "button-text-color": "#fcfcfc",
                        "button-border-color": "#dddddd",
                        "input-background-color": "#fcfcfc",
                        "input-text-color": "#111111",
                        "input-placeholder-color": "#111111"
                    };

                    $('.payment-redirect-form', context).on('submit', function () {
                      $("#bModal").fadeIn('fast');
                      Bancard.Cards.createForm('iframe-container', options, styles);

                      return false;
                    });

                    $('.b-close', context).on('click', function () {
                      $("#bModal").fadeOut('fast');

                      return false;
                    });

                    $('#bModal', context).on('click', function () {
                      $("#bModal").fadeOut('fast');

                      return false;
                    });
                  }
                });
              }
  
    };
  
  }(jQuery, Drupal, drupalSettings, once));