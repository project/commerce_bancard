<?php

namespace Drupal\commerce_bancard\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;

interface BancardStoredPaymentGatewayInterface extends SupportsStoredPaymentMethodsInterface, SupportsCardNotificationsInterface {

    /**
     * Get the configured Bancard API Public key.
     *
     * @return string
     *   The Bancard API Public key.
     */
    public function getPublicKey();

    /**
     * Get the configured Bancard API Private key.
     *
     * @return string
     *   The Bancard API Private key.
     */
    public function getPrivateKey();
}
